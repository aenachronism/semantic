package semantic

import scala.collection.{mutable}
import scalaz.syntax.std.option._

/**
  * Created by aenachronism on 2/10/16.
  */
class BloomFilter[A <: AnyRef](multiHash: MultiHashFn) {
  val index                               = mutable.BitSet.empty
  var buffer: Option[(A, HashCollection)] = None

  /**
    * Determines whether a given element is present in the filter.  As a performance optimisation, we store
    * the computed hashes for the element based on the assumption that client code will invoke contains(), then add()
    * in the cases where the element does not exist.  Accordingly, we momentarily store the last computed hashes to
    * amortize the cost of the hashing function.
    *
    * @param a The element to locate
    * @return True if the element is present, false otherwise.
    */
  def contains(a: A): Boolean = {
    val hashes: HashCollection = multiHash(a)
    buffer = Some((a, hashes))
    hashes forall index.contains
  }

  /**
    * Adds the given element to the filter.  This implementation returns <code>Unit</code>.  Arguably it could return a
    * <code>Boolean</code> value, with <code>true</code> indicating that the element did not previously exist in the
    * filter, false otherwise.  This would effectively become a side-effecting contains call and probably not worth it.
    *
    * @param a The element to add
    */
  def add(a: A): Unit = {
    val hashes = getBufferedHashes(a) | multiHash(a)
    hashes foreach index.add
  }

  /**
    * Utility function that will retrieve the hashes for the given element a if they exist
    * @param a The element for which we wishes to retrieve the hashes
    * @return Some(hashes) if we find a match, None otherwise
    */
  private def getBufferedHashes(a: A): Option[HashCollection] = {
    val buf = buffer
    buf.flatMap { case (a2, hashes) ⇒ if (a == a2) Some(hashes) else None }
  }
}


