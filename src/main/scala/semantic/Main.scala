package semantic

import scala.io.Source
import scalaz.syntax.std.option._

object Main {
  val log = org.slf4j.LoggerFactory.getLogger(getClass)

  private def usage() {
    println("""
      |Usage:
      |$ sbt 'run [--hashes num] [--algo SHA|MD5] filename'
      |
      |where:
      |
      |  --algo : the algorithm to use as the basis of the hashing function. If not specified, MD5 is assumed.
      |  --hashes: the number of hashes to generate.  If not specified, the default is 2.
      |""".stripMargin)
  }

  type OptionMap = Map[Symbol, Any]

  def getOptions(args: Array[String]): OptionMap = {
    val argsList = args.toList

    def options(map: OptionMap, list: List[String]) : OptionMap = {
      list match {
        case Nil ⇒ map
        case "--hashes" :: num :: tail  ⇒ options(map ++ Map('count → num.toInt), tail)
        case "--algo" :: "SHA" :: tail ⇒ options(map ++ Map('algo → semantic.SHA1 ), tail)
        case "--algo" :: "MD5" :: tail ⇒ options(map ++ Map('algo → semantic.MD5 ), tail)
        case string :: Nil             ⇒ map ++ Map('fname → string)
        case option :: tail            ⇒ log.warn("Ignoring unknown option [$option]"); options(map, tail)
      }
    }

    options(Map(),argsList)
  }

  def processFile(count: Int, algo: Algorithm, f: String): Unit = {
    var lines               = Source.fromFile(f).getLines()
    val filter              = new BloomFilter[String](semantic.makeMultiHash(count, algo))
    val (collisions, total) = lines.foldLeft((0, 0)) {
      case ((cs, t), word) ⇒
        if (filter.contains(word))
          (cs + 1, t + 1)
        else {
          filter.add(word)
          (cs, t + 1)
        }
    }

    // Since we get an iterator from getLines ... we need to grab a new one every time ... my Kingdom for an it.rewind()
    lines = Source.fromFile(f).getLines()
    val found = lines.foldLeft(0) {
      (count, word) => if (filter.contains(word)) count + 1 else count
    }

    log.info("Detected {} collisions out of a total of {} words", collisions, total)
    log.info("Found {} words out of a total of {}", found, total)
  }

  def main(args: Array[String]) {
    if (args.length == 0) usage() else {
      val options = getOptions(args)
      val count   = (options.get('count) | 2).asInstanceOf[Int]
      val algo    = (options.get('algo) | semantic.MD5).asInstanceOf[semantic.Algorithm]
      val file    = options.get('fname)

      file match {
        case Some(f: String) ⇒ processFile(count, algo, f)
        case _               ⇒ usage()
      }
    }
  }
}
