/**
  * Created by aenachronism on 2/10/16.
  */
package object semantic {
  type HashCollection = Seq[Int]
  type MultiHashFn    = AnyRef => HashCollection

  /**
    * The algorithms that we support as the basis for the multi hash function.
    */
  sealed trait Algorithm {
    /**
      * The name of the algorithm that can be used to instantiate a java.security.MessageDigest instance
      */
    val name: String
    /**
      * Output length in bytes of the digest() function
      */
    val length: Int
  }

  case object MD5 extends Algorithm {
    val name   = "MD5"
    val length = 16
  }

  case object SHA1 extends Algorithm {
    val name   = "SHA-1"
    val length = 20
  }

  /**
    * Converts an array of bytes into Int values by relying heavily on java.nio functionality
    *
    * Since we are converting to Int values, we are careful to not overflow the maximum permissible values for Ints,
    * since that would result in unnecessary collisions when these values are used as hashes.
    */
  object Convert {
    import java.nio.ByteBuffer

    /**
      * We want to be sure that we do not overflow integers.  We're pretty much assured that the underlying VM will
      * always have 32 bit Ints (4 bytes), but this way, if it ever changes, we won't be impacted :D
      */
    val maxDigits = BigInt(Int.MinValue).toByteArray.length

    /**
      * Converts an array of bytes into a positive integer
      *
      * @param bytes The byte array to convert.  Note that only the N bytes of the array are used, where N is the
      *              platforms native Int byte size ( typically 4 )
      *
      * @param idx The index into the byte array from which we start taking the N bytes to convert
      * @return A positive integer of the byte representation.  Since we are inserting these values into a
      */
    def bytesToInt(bytes: Array[Byte], idx: Int): Int = {
      math.abs(ByteBuffer.wrap(bytes).getInt(idx))
    }
  }


  /**
    * Create a hashing function that will provide <code>count</code> hashes for a reference by converting the reference
    * to a string, and then taking a message digest of that string to introduce randomness and uniformity in the
    * distribution of values.  A sliding window the size of the platform's native Int type ( typically 4 bytes ) is then
    * used to generate the individual hashes.
    *
    * That is, hash(0) comprises bytes 0, 1, 2, and 3 of the message digest, hash(1) comprises bytes 1, 2, 3, and 4, and
    * so on.
    *
    * Note that as a result of this sliding window, the count is practically limited to the number of bytes returned
    * from the message digest function less the byte size needed to represent Int on the platform.  Accordingly, for the
    * MD5 algorithm, we can maximally generate 12 hashes from its 16 byte message digest, whereas SHA-1 generates 20
    * byte digests, allowing for the potential generation of 16 hashes.
    *
    * @param count The number of hashes to perform.  The minimum value is 1, the maximum value depends on the algorithm
    *              specified as explained above.
    *
    * @param algo The algorithm to use as the basis for the hashes
    *
    * @return A function that when invoked will produce a sequence of <code>count</code> hashes for a given reference.
    */
  def makeMultiHash(count: Int, algo: Algorithm = MD5): MultiHashFn = a => {
    import java.security.MessageDigest

    require(count > 0)

    val maxCount = algo.length - Convert.maxDigits
    val md       = MessageDigest.getInstance(algo.name)

    md.update(a.toString.getBytes())
    val bytes: Array[Byte] = md.digest()

    (0 until (count % maxCount)).map(Convert.bytesToInt(bytes, _))
  }
}
