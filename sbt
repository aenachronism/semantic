#!/bin/sh
exec java -Dfile.encoding=UTF8 -server -Xms512m -Xmx2048m -Xss1m -XX:+CMSClassUnloadingEnabled -jar `dirname $0`/bin/sbt-launch.jar "$@"
